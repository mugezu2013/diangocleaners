from django.contrib import admin
from .models import Booking


@admin.register(Booking)
class BookingAdminModel(admin.ModelAdmin):
    pass
# Register your models here.
