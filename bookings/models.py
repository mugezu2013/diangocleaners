from django.db import models

from cites.models import City
from customers.models import Customer
from cleaners.models import Cleaner


class Booking(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    cleaner = models.ForeignKey(Cleaner, on_delete=models.CASCADE)
    date = models.DateTimeField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    price = models.DecimalField(decimal_places=2, max_digits=6)
