from django.core.exceptions import ValidationError
from django import forms
from django.core.validators import RegexValidator
from datetime import timedelta
from django.utils import timezone
from bookings.const import TIME_CLEANING
from cites.models import City
from cleaners.models import Cleaner

#     todo
class BookingForm(forms.Form):
    first_name = forms.CharField(max_length=100, label='Имя')
    last_name = forms.CharField(max_length=100, label='Фамилия')
    phone_number = forms.CharField(
        max_length=15,
        validators=[RegexValidator(r"[+]?\d{9,15}", message='Неверный формат телефона')],
        label="Номер телефона"
        )

    city = forms.ModelChoiceField(queryset=City.objects.all(), widget=forms.Select, label='Город')
    date = forms.DateTimeField(label='Дата и время')

    def clean(self):

        # функция подсчёта минимальной даты, когда клиент не может сделать заказ на нужное ему время
        # принимает клинеров
        # возвращает минимальную дату, когда можно сдлеать заказ
        def get_near_date(cleaners):
            result = []

            # делаем запрос в бд по каждому клинеру и получаем заказы, которые больше даты, которую ввел пользователь
            for cleaner in cleaners.iterator():
                dates_bookings = cleaner.booking_set.filter(
                    city=cd['city'],
                    date__gt=min_date
                ).order_by('date')

                # добавляем к результату свободное время для каждого клинера
                result.append(get_near_date_cleaner(dates_bookings))

            # сортируем для получения минимальной даты
            result.sort()
            return result[0]

        # функция определения минимальной даты взятия заказа для клинера
        # возвращает ближайшее время, когда клинер сможет взять заказ
        def get_near_date_cleaner(dates_bookings_cleaner):
            # начальные значения
            # date_pre - запоминает предедущую дату заказа
            # flag - для определения было ли достаточное окно между предыдущим и препредыдущим заказом

            date_pre = min_date - delta
            flag = False
            for booking in dates_bookings_cleaner.iterator():
                # если было окно в предедыщем случае и есть окно до следующего заказа -
                # возвращаем дату передыдущего заказа + окно работы
                if flag and (booking.date - delta) - date_pre >= delta:
                    return date_pre + delta
                # обнуляем флаг
                elif flag:
                    flag = False
                # преверяем есть ли необходимое окно для текущей итерации
                if booking.date - date_pre >= delta:
                    flag = True
                date_pre = booking.date
            # если прошли все заказы, но так и не нашли время для заказа -
            # берем последний и добавляем к нему окно работы
            else:
                return booking.date + delta
        cd = super().clean()

        # провермяем на наличие даты(если она оказалась не валидной)
        try:
            date = cd['date']
        except KeyError:
            return cd

        # проверка, если дата с прошлого
        if date < (timezone.now()+timedelta(hours=3)):
            raise ValidationError("Введите дату больше текущей")
        delta = timedelta(seconds=TIME_CLEANING)
        min_date = date - delta
        max_date = date + delta

        # запрос в бд для нахождения клинера
        # делаем по городу, исключая время когда он уже занят
        cleaner = (
            Cleaner
                .objects
                .filter(cities__in=[cd['city']])
                .exclude(booking__date__range=[min_date, max_date]).first()
        )

        # если есть свободный клинера на данное время  - добавляем его в cd
        if cleaner:
            self.cleaned_data['cleaner'] = cleaner

        # иначе ищем ближайшее время для заказа и предлагаем его пользователю
        else:
            cleaners = Cleaner.objects.prefetch_related('booking_set').filter(
                booking__city=cd['city']).distinct()
            d = get_near_date(cleaners)
            raise ValidationError(f'Нет свободных клинеров на данное время в вашем городе. Ближайшее время {d}')
        return self.cleaned_data
