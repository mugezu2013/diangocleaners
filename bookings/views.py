import datetime
import decimal

from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.edit import FormView
from bookings.forms import BookingForm
from bookings.models import Booking
from cleaners.models import Cleaner
from customers.models import Customer
from .const import SALE, TIME_RATING
from django.contrib import messages


class BookingCreate(FormView):
    template_name = 'bookings/booking_form.html'
    form_class = BookingForm
    success_url = reverse_lazy('main:')
    sale = 0

    def form_valid(self, form):
        cd = form.cleaned_data

        # проверям номер на наличие в БД и делаем скидку
        try:
            customer = Customer.objects.get(phone_number=cd['phone_number'])
            self.sale = SALE

        # иначе добавляем его в бд
        except Customer.DoesNotExist:
            customer = Customer.objects.create(
                first_name=cd['first_name'],
                last_name=cd['last_name'],
                phone_number=cd['phone_number']
            )
        # считаем цену
        price = decimal.Decimal(cd['city'].price * decimal.Decimal(1 - self.sale))

        # добавляем заказ в БД
        Booking.objects.create(
            price=price,
            cleaner=cd['cleaner'],
            date=cd['date'],
            city=cd['city'],
            customer=customer
        )

        # пересчет рейтинга для клинера, что принял заказ
        # количество чисток за последние 2 дня
        bookings_count = Booking.objects.filter(
            cleaner=cd['cleaner'],
            date__range=[
                (datetime.datetime.now() - datetime.timedelta(hours=TIME_RATING)),
                datetime.datetime.now()])\
            .count()

        # считаем если за последние 2 дня чистки были
        if bookings_count != 0:

            # количество чисток за 2 дня * 2 / на время подсчёта рейтинга * 100
            rating = bookings_count*2 / TIME_RATING * 100

            # обновление записи в бд
            Cleaner.objects.filter(id=cd['cleaner'].id).update(quality_score=rating)

        messages.success(self.request, 'Заказ подтвержден. Cтоимость ' + str(float(price)))

        return redirect(self.success_url)
