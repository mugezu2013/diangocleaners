from django.urls import path

from bookings.views import BookingCreate

app_name = 'bookings'

urlpatterns = [
    path('booking/new/', BookingCreate.as_view(), name='new')
]
