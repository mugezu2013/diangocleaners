from django.urls import path

from .views import (
    CleanerList,
    CleanerDetail,
    CleanerCreation,
    CleanerUpdate,
    CleanerDelete
)

app_name = 'cleaners'

urlpatterns = [

    path('cleaners/', CleanerList.as_view(), name='list'),
    path('cleaner/<int:pk>/', CleanerDetail.as_view(), name='detail'),
    path('cleaner/new/', CleanerCreation.as_view(), name='new'),
    path('cleaner/edit/<int:pk>/', CleanerUpdate.as_view(), name='edit'),
    path('cleaner/delete/<int:pk>/', CleanerDelete.as_view(), name='delete'),

]
