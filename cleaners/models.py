from django.db import models

from cites.models import City


class Cleaner(models.Model):
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    quality_score = models.DecimalField(max_digits=5, decimal_places=2, default=100, verbose_name='Рейтинг')
    number_phone = models.CharField(max_length=15, verbose_name="Номер телефона")
    cities = models.ManyToManyField(City, verbose_name="Города")

    class Meta:
        ordering = ['-quality_score']
        verbose_name = 'Клинер'
        verbose_name_plural = 'Клинеры'
