from django.contrib import admin
from .models import Cleaner


@admin.register(Cleaner)
class CleanerAdminModel(admin.ModelAdmin):
    list_display = ['first_name', 'last_name']

# admin.site.register(Cleaner)


# Register your models here.
