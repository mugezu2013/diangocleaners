from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Cleaner


class CleanerList(ListView):
    model = Cleaner
    paginate_by = 5

    def get_context_data(self):
        context = super().get_context_data()
        context['title'] = 'Список клинеров'
        return context


class CleanerDetail(DetailView):
    model = Cleaner


class CleanerCreation(CreateView):
    model = Cleaner
    success_url = reverse_lazy('cleaners:list')
    fields = ['first_name', 'last_name', 'cities']

    def get_context_data(self):
        context = super().get_context_data()
        context['title'] = 'Добавить клинера'
        return context

    def post(self, request, *args, **kwargs):
        messages.success(self.request, 'Клинер добавлен')
        return super().post(request, *args, **kwargs)

class CleanerUpdate(UpdateView):
    model = Cleaner
    success_url = reverse_lazy('cleaners:list')
    fields = ['first_name', 'last_name', 'cities']

    def post(self, request, *args, **kwargs):
        messages.success(self.request, 'Клинер обновлен')
        return super().post(request, *args, **kwargs)

    def get_context_data(self):
        context = super().get_context_data()
        context['title'] = 'Обновить клинера'
        return context


class CleanerDelete(DeleteView):
    template_name = 'cleaners/cleaner_list.html'
    model = Cleaner
    success_url = reverse_lazy('cleaners:list')

    def post(self, request, *args, **kwargs):
        messages.success(self.request, 'Клинер удален')
        return super().post(request, *args, **kwargs)
