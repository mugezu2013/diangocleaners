from django.urls import path

from .views import (
    CustomerList,
    CustomerDetail,
    CustomerCreation,
    CustomerUpdate,
    CustomerDelete
)

app_name = 'customers'

urlpatterns = [

    path('customers/', CustomerList.as_view(), name='list'),
    path('customer/<int:pk>/', CustomerDetail.as_view(), name='detail'),
    path('customer/new/', CustomerCreation.as_view(), name='new'),
    path('customer/edit/<int:pk>/', CustomerUpdate.as_view(), name='edit'),
    path('customer/delete/<int:pk>/', CustomerDelete.as_view(), name='delete'),

]