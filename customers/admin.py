from django.contrib import admin
from customers.models import Customer


@admin.register(Customer)
class CustomerAdminModel(admin.ModelAdmin):
    pass


# Register your models here.
