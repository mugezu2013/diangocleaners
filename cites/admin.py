from django.contrib import admin
from cites.models import City


@admin.register(City)
class CityAdminModel(admin.ModelAdmin):
    pass


# Register your models here.
