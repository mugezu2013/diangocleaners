from django.db import models


class City(models.Model):
    title = models.CharField(max_length=30, verbose_name='Название города')
    price = models.DecimalField(max_digits=6, decimal_places=2, verbose_name='Стоимость клининга')

    def __str__(self):
        return self.title

# Create your models here.
